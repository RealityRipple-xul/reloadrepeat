# ReloadRepeat

The ReloadRepeat extension adds the funcionality to reload the web page you are viewing every so many seconds or minutes.

#### Supports
 * Pale Moon [33.0 - 33.*]

## Building
Simply download the contents of the repository and pack the contents (sans git data) into a .zip file. Then, rename the file to .xpi and drag into the browser.

## Download
You can grab the latest release from the [Official Web Site](//realityripple.com/Software/XUL/ReloadRepeat/).
